#ifndef SINALESCOLAR_H
#define SINALESCOLAR_H

#include <QMainWindow>
#include <QMediaPlayer>

namespace Ui {
class SinalEscolar;
}

class SinalEscolar : public QMainWindow
{
    Q_OBJECT

public:
    explicit SinalEscolar(QWidget *parent = 0);
    ~SinalEscolar();

private slots:
    void showTime();

    void RColName();
    void RHorManha();
    void RHorTarde();
    void RHorNoite();
    void TestaHorario();

    void on_btnSair_clicked();

    void on_btn_PlayLonga_clicked();


    void on_ck_Silencio_clicked();

    void on_btn_StopLonga_clicked();

    void on_btn_PlayCurta_clicked();

    void on_btn_PlayLeit_clicked();

    void on_btn_PlayHino_clicked();

    void on_btn_AltSongs_clicked();

    void on_btn_playSirene_clicked();

    void VPLonga();
    void VPCurta();
    void VPLeitura();
private:
    Ui::SinalEscolar *ui;
    bool shii=false;
    int auxvvl=0;
    int Hnow=0;

    int vvl_m[8];
    int vvl_t[8];
    int vvl_n[6];

    QMediaPlayer *playLonga = new QMediaPlayer;
    QMediaPlayer *playCurta = new QMediaPlayer;
    QMediaPlayer *playLeitura = new QMediaPlayer;
    QMediaPlayer *playHino = new QMediaPlayer;
    QMediaPlayer *playSirene = new QMediaPlayer;


};

#endif // SINALESCOLAR_H
