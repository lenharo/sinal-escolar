#include "sinalescolar.h"
#include "ui_sinalescolar.h"
#include "musicas.h"

#include <QDesktopWidget>
#include <QFile>
#include <QTextStream>
#include <QUrl>
#include <QTimer>
#include <QDateTime>

#include <QMediaPlayer>

SinalEscolar::SinalEscolar(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::SinalEscolar)
{
    ui->setupUi(this);
    //Centraliza a tela na abertura
    QRect src = QApplication::desktop()->screenGeometry();
    move(src.center() - rect().center());



    this->centralWidget()->setStyleSheet("background-image:url(\":/new/imgs/back_950.jpeg\"); background-position: center; ");

    //Le arquivo com Nome do Colégio
    RColName();
    RHorManha();
    RHorTarde();
    RHorNoite();

    QTimer *timer=new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(showTime()) );
    timer->start();


}


void SinalEscolar::showTime()
{
    QTime time=QTime::currentTime();
    QString time_text=time.toString("hh : mm : ss");
    ui->lbl_Relogio->setText(time_text);
    TestaHorario();
}

void SinalEscolar::RColName()
{
    //Le arquivo com Nome do Colégio
    QFile inputFileNomeCol("/opt/Sinal/NomeColegio.txt");
    if (inputFileNomeCol.open(QIODevice::ReadOnly))
    {
       QTextStream in(&inputFileNomeCol);
       while (!in.atEnd())
       {
          QString line = in.readLine();
          ui->lbl_NomeCol->setText(line);


       }
       inputFileNomeCol.close();
    }
}

void SinalEscolar::RHorManha()
{
    //Le arquivo com horários da Manha
    QFile inputFileManha("/opt/Sinal/Horario/manha.txt");
    if (inputFileManha.open(QIODevice::ReadOnly))
    {
        int i=0;
       QTextStream in(&inputFileManha);
       while (!in.atEnd())
       {
          QString line = in.readLine();
          switch (i) {
          case 0:
              ui->lbl_em01->setText(line);

              auxvvl = ui->lbl_em01->text().split("")[1].toInt();
              vvl_m[0]=auxvvl*10*3600;
              auxvvl = ui->lbl_em01->text().split("")[2].toInt();
              vvl_m[0]=vvl_m[0]+(auxvvl*3600);
              auxvvl = ui->lbl_em01->text().split("")[4].toInt();
              vvl_m[0]=vvl_m[0]+(auxvvl*10*60);
              auxvvl = ui->lbl_em01->text().split("")[5].toInt();
              vvl_m[0]=vvl_m[0]+(auxvvl*60);

              break;
          case 1:
              ui->lbl_em02->setText(line);
              ui->lbl_sm01->setText(line);

              auxvvl = ui->lbl_em02->text().split("")[1].toInt();
              vvl_m[1]=auxvvl*10*3600;
              auxvvl = ui->lbl_em02->text().split("")[2].toInt();
              vvl_m[1]=vvl_m[1]+(auxvvl*3600);
              auxvvl = ui->lbl_em02->text().split("")[4].toInt();
              vvl_m[1]=vvl_m[1]+(auxvvl*10*60);
              auxvvl = ui->lbl_em02->text().split("")[5].toInt();
              vvl_m[1]=vvl_m[1]+(auxvvl*60);

              break;
          case 2:
              ui->lbl_em03->setText(line);
              ui->lbl_sm02->setText(line);

              auxvvl = ui->lbl_em03->text().split("")[1].toInt();
              vvl_m[2]=auxvvl*10*3600;
              auxvvl = ui->lbl_em03->text().split("")[2].toInt();
              vvl_m[2]=vvl_m[2]+(auxvvl*3600);
              auxvvl = ui->lbl_em03->text().split("")[4].toInt();
              vvl_m[2]=vvl_m[2]+(auxvvl*10*60);
              auxvvl = ui->lbl_em03->text().split("")[5].toInt();
              vvl_m[2]=vvl_m[2]+(auxvvl*60);

              break;
          case 3:
              ui->lbl_em04->setText(line);
              ui->lbl_sm03->setText(line);

              auxvvl = ui->lbl_em04->text().split("")[1].toInt();
              vvl_m[3]=auxvvl*10*3600;
              auxvvl = ui->lbl_em04->text().split("")[2].toInt();
              vvl_m[3]=vvl_m[3]+(auxvvl*3600);
              auxvvl = ui->lbl_em04->text().split("")[4].toInt();
              vvl_m[3]=vvl_m[3]+(auxvvl*10*60);
              auxvvl = ui->lbl_em04->text().split("")[5].toInt();
              vvl_m[3]=vvl_m[3]+(auxvvl*60);

              break;
          case 4:
              ui->lbl_em05->setText(line);
              ui->lbl_sm04->setText(line);

              auxvvl = ui->lbl_em05->text().split("")[1].toInt();
              vvl_m[4]=auxvvl*10*3600;
              auxvvl = ui->lbl_em05->text().split("")[2].toInt();
              vvl_m[4]=vvl_m[4]+(auxvvl*3600);
              auxvvl = ui->lbl_em05->text().split("")[4].toInt();
              vvl_m[4]=vvl_m[4]+(auxvvl*10*60);
              auxvvl = ui->lbl_em05->text().split("")[5].toInt();
              vvl_m[4]=vvl_m[4]+(auxvvl*60);
              break;
          case 5:
              ui->lbl_em06->setText(line);
              ui->lbl_sm05->setText(line);

              auxvvl = ui->lbl_em06->text().split("")[1].toInt();
              vvl_m[5]=auxvvl*10*3600;
              auxvvl = ui->lbl_em06->text().split("")[2].toInt();
              vvl_m[5]=vvl_m[5]+(auxvvl*3600);
              auxvvl = ui->lbl_em06->text().split("")[4].toInt();
              vvl_m[5]=vvl_m[5]+(auxvvl*10*60);
              auxvvl = ui->lbl_em06->text().split("")[5].toInt();
              vvl_m[5]=vvl_m[5]+(auxvvl*60);
              break;
          case 6:
              ui->lbl_em07->setText(line);
              ui->lbl_sm06->setText(line);

              auxvvl = ui->lbl_em07->text().split("")[1].toInt();
              vvl_m[6]=auxvvl*10*3600;
              auxvvl = ui->lbl_em07->text().split("")[2].toInt();
              vvl_m[6]=vvl_m[6]+(auxvvl*3600);
              auxvvl = ui->lbl_em07->text().split("")[4].toInt();
              vvl_m[6]=vvl_m[6]+(auxvvl*10*60);
              auxvvl = ui->lbl_em07->text().split("")[5].toInt();
              vvl_m[6]=vvl_m[6]+(auxvvl*60);
              break;
          case 7:
              ui->lbl_sm07->setText(line);

              auxvvl = ui->lbl_sm07->text().split("")[1].toInt();
              vvl_m[7]=auxvvl*10*3600;
              auxvvl = ui->lbl_sm07->text().split("")[2].toInt();
              vvl_m[7]=vvl_m[7]+(auxvvl*3600);
              auxvvl = ui->lbl_sm07->text().split("")[4].toInt();
              vvl_m[7]=vvl_m[7]+(auxvvl*10*60);
              auxvvl = ui->lbl_sm07->text().split("")[5].toInt();
              vvl_m[7]=vvl_m[7]+(auxvvl*60);
          default:
              break;

          }
          i++;

       }
       inputFileManha.close();
    }

}

void SinalEscolar::RHorTarde()
{
    //Le arquivo com horários da Tarde
    QFile inputFileTarde("/opt/Sinal/Horario/tarde.txt");
    if (inputFileTarde.open(QIODevice::ReadOnly))
    {
        int i=0;
       QTextStream in(&inputFileTarde);
       while (!in.atEnd())
       {
          QString line = in.readLine();
          switch (i) {
          case 0:
              ui->lbl_et01->setText(line);

              auxvvl = ui->lbl_et01->text().split("")[1].toInt();
              vvl_t[0]=auxvvl*10*3600;
              auxvvl = ui->lbl_et01->text().split("")[2].toInt();
              vvl_t[0]=vvl_t[0]+(auxvvl*3600);
              auxvvl = ui->lbl_et01->text().split("")[4].toInt();
              vvl_t[0]=vvl_t[0]+(auxvvl*10*60);
              auxvvl = ui->lbl_et01->text().split("")[5].toInt();
              vvl_t[0]=vvl_t[0]+(auxvvl*60);
              break;
          case 1:
              ui->lbl_et02->setText(line);
              ui->lbl_st01->setText(line);

              auxvvl = ui->lbl_et02->text().split("")[1].toInt();
              vvl_t[1]=auxvvl*10*3600;
              auxvvl = ui->lbl_et02->text().split("")[2].toInt();
              vvl_t[1]=vvl_t[1]+(auxvvl*3600);
              auxvvl = ui->lbl_et02->text().split("")[4].toInt();
              vvl_t[1]=vvl_t[1]+(auxvvl*10*60);
              auxvvl = ui->lbl_et02->text().split("")[5].toInt();
              vvl_t[1]=vvl_t[1]+(auxvvl*60);
              break;
          case 2:
              ui->lbl_et03->setText(line);
              ui->lbl_st02->setText(line);

              auxvvl = ui->lbl_et03->text().split("")[1].toInt();
              vvl_t[2]=auxvvl*10*3600;
              auxvvl = ui->lbl_et03->text().split("")[2].toInt();
              vvl_t[2]=vvl_t[2]+(auxvvl*3600);
              auxvvl = ui->lbl_et03->text().split("")[4].toInt();
              vvl_t[2]=vvl_t[2]+(auxvvl*10*60);
              auxvvl = ui->lbl_et03->text().split("")[5].toInt();
              vvl_t[2]=vvl_t[2]+(auxvvl*60);
              break;
          case 3:
              ui->lbl_et04->setText(line);
              ui->lbl_st03->setText(line);

              auxvvl = ui->lbl_et04->text().split("")[1].toInt();
              vvl_t[3]=auxvvl*10*3600;
              auxvvl = ui->lbl_et04->text().split("")[2].toInt();
              vvl_t[3]=vvl_t[3]+(auxvvl*3600);
              auxvvl = ui->lbl_et04->text().split("")[4].toInt();
              vvl_t[3]=vvl_t[3]+(auxvvl*10*60);
              auxvvl = ui->lbl_et04->text().split("")[5].toInt();
              vvl_t[3]=vvl_t[3]+(auxvvl*60);
              break;
          case 4:
              ui->lbl_et05->setText(line);
              ui->lbl_st04->setText(line);

              auxvvl = ui->lbl_et05->text().split("")[1].toInt();
              vvl_t[4]=auxvvl*10*3600;
              auxvvl = ui->lbl_et05->text().split("")[2].toInt();
              vvl_t[4]=vvl_t[4]+(auxvvl*3600);
              auxvvl = ui->lbl_et05->text().split("")[4].toInt();
              vvl_t[4]=vvl_t[4]+(auxvvl*10*60);
              auxvvl = ui->lbl_et05->text().split("")[5].toInt();
              vvl_t[4]=vvl_t[4]+(auxvvl*60);
              break;
          case 5:
              ui->lbl_et06->setText(line);
              ui->lbl_st05->setText(line);

              auxvvl = ui->lbl_et06->text().split("")[1].toInt();
              vvl_t[5]=auxvvl*10*3600;
              auxvvl = ui->lbl_et06->text().split("")[2].toInt();
              vvl_t[5]=vvl_t[5]+(auxvvl*3600);
              auxvvl = ui->lbl_et06->text().split("")[4].toInt();
              vvl_t[5]=vvl_t[5]+(auxvvl*10*60);
              auxvvl = ui->lbl_et06->text().split("")[5].toInt();
              vvl_t[5]=vvl_t[5]+(auxvvl*60);
              break;
          case 6:
              ui->lbl_et07->setText(line);
              ui->lbl_st06->setText(line);

              auxvvl = ui->lbl_et07->text().split("")[1].toInt();
              vvl_t[6]=auxvvl*10*3600;
              auxvvl = ui->lbl_et07->text().split("")[2].toInt();
              vvl_t[6]=vvl_t[6]+(auxvvl*3600);
              auxvvl = ui->lbl_et07->text().split("")[4].toInt();
              vvl_t[6]=vvl_t[6]+(auxvvl*10*60);
              auxvvl = ui->lbl_et07->text().split("")[5].toInt();
              vvl_t[6]=vvl_t[6]+(auxvvl*60);
              break;
          case 7:
              ui->lbl_st07->setText(line);

              auxvvl = ui->lbl_st07->text().split("")[1].toInt();
              vvl_t[7]=auxvvl*10*3600;
              auxvvl = ui->lbl_st07->text().split("")[2].toInt();
              vvl_t[7]=vvl_t[7]+(auxvvl*3600);
              auxvvl = ui->lbl_st07->text().split("")[4].toInt();
              vvl_t[7]=vvl_t[7]+(auxvvl*10*60);
              auxvvl = ui->lbl_st07->text().split("")[5].toInt();
              vvl_t[7]=vvl_t[7]+(auxvvl*60);
          default:
              break;

          }
          i++;

       }
       inputFileTarde.close();
    }


}

void SinalEscolar::RHorNoite()
{
    //Le arquivo com horários da Noite
    QFile inputFileNoite("/opt/Sinal/Horario/noite.txt");
    if (inputFileNoite.open(QIODevice::ReadOnly))
    {
        int i=0;
       QTextStream in(&inputFileNoite);
       while (!in.atEnd())
       {
          QString line = in.readLine();
          switch (i) {
          case 0:
              ui->lbl_en01->setText(line);

              auxvvl = ui->lbl_en01->text().split("")[1].toInt();
              vvl_n[0]=auxvvl*10*3600;
              auxvvl = ui->lbl_en01->text().split("")[2].toInt();
              vvl_n[0]=vvl_n[0]+(auxvvl*3600);
              auxvvl = ui->lbl_en01->text().split("")[4].toInt();
              vvl_n[0]=vvl_n[0]+(auxvvl*10*60);
              auxvvl = ui->lbl_en01->text().split("")[5].toInt();
              vvl_n[0]=vvl_n[0]+(auxvvl*60);
              break;
          case 1:
              ui->lbl_en02->setText(line);
              ui->lbl_sn01->setText(line);

              auxvvl = ui->lbl_en02->text().split("")[1].toInt();
              vvl_n[1]=auxvvl*10*3600;
              auxvvl = ui->lbl_en02->text().split("")[2].toInt();
              vvl_n[1]=vvl_n[1]+(auxvvl*3600);
              auxvvl = ui->lbl_en02->text().split("")[4].toInt();
              vvl_n[1]=vvl_n[1]+(auxvvl*10*60);
              auxvvl = ui->lbl_en02->text().split("")[5].toInt();
              vvl_n[1]=vvl_n[1]+(auxvvl*60);
              break;
          case 2:
              ui->lbl_en03->setText(line);
              ui->lbl_sn02->setText(line);

              auxvvl = ui->lbl_en03->text().split("")[1].toInt();
              vvl_n[2]=auxvvl*10*3600;
              auxvvl = ui->lbl_en03->text().split("")[2].toInt();
              vvl_n[2]=vvl_n[2]+(auxvvl*3600);
              auxvvl = ui->lbl_en03->text().split("")[4].toInt();
              vvl_n[2]=vvl_n[2]+(auxvvl*10*60);
              auxvvl = ui->lbl_en03->text().split("")[5].toInt();
              vvl_n[2]=vvl_n[2]+(auxvvl*60);
              break;
          case 3:
              ui->lbl_en04->setText(line);
              ui->lbl_sn03->setText(line);

              auxvvl = ui->lbl_en04->text().split("")[1].toInt();
              vvl_n[3]=auxvvl*10*3600;
              auxvvl = ui->lbl_en04->text().split("")[2].toInt();
              vvl_n[3]=vvl_n[3]+(auxvvl*3600);
              auxvvl = ui->lbl_en04->text().split("")[4].toInt();
              vvl_n[3]=vvl_n[3]+(auxvvl*10*60);
              auxvvl = ui->lbl_en04->text().split("")[5].toInt();
              vvl_n[3]=vvl_n[3]+(auxvvl*60);
              break;
          case 4:
              ui->lbl_en05->setText(line);
              ui->lbl_sn04->setText(line);

              auxvvl = ui->lbl_en05->text().split("")[1].toInt();
              vvl_n[4]=auxvvl*10*3600;
              auxvvl = ui->lbl_en05->text().split("")[2].toInt();
              vvl_n[4]=vvl_n[4]+(auxvvl*3600);
              auxvvl = ui->lbl_en05->text().split("")[4].toInt();
              vvl_n[4]=vvl_n[4]+(auxvvl*10*60);
              auxvvl = ui->lbl_en05->text().split("")[5].toInt();
              vvl_n[4]=vvl_n[4]+(auxvvl*60);
              break;
          case 5:
              ui->lbl_en06->setText(line);
              ui->lbl_sn05->setText(line);

              auxvvl = ui->lbl_en06->text().split("")[1].toInt();
              vvl_n[5]=auxvvl*10*3600;
              auxvvl = ui->lbl_en06->text().split("")[2].toInt();
              vvl_n[5]=vvl_n[5]+(auxvvl*3600);
              auxvvl = ui->lbl_en06->text().split("")[4].toInt();
              vvl_n[5]=vvl_n[5]+(auxvvl*10*60);
              auxvvl = ui->lbl_en06->text().split("")[5].toInt();
              vvl_n[5]=vvl_n[5]+(auxvvl*60);
              break;
          case 6:
              ui->lbl_sn06->setText(line);

              auxvvl = ui->lbl_sn06->text().split("")[1].toInt();
              vvl_n[6]=auxvvl*10*3600;
              auxvvl = ui->lbl_sn06->text().split("")[2].toInt();
              vvl_n[6]=vvl_n[6]+(auxvvl*3600);
              auxvvl = ui->lbl_sn06->text().split("")[4].toInt();
              vvl_n[6]=vvl_n[6]+(auxvvl*10*60);
              auxvvl = ui->lbl_sn06->text().split("")[5].toInt();
              vvl_n[6]=vvl_n[6]+(auxvvl*60);
              break;

          default:
              break;

          }
          i++;

       }
       inputFileNoite.close();
    }

}

void SinalEscolar::TestaHorario()
{
    //Converte Horario Atual para segundos e guarda em Hnow
    auxvvl = ui->lbl_Relogio->text().split("")[1].toInt();
    Hnow=auxvvl*10*3600;
    auxvvl = ui->lbl_Relogio->text().split("")[2].toInt();
    Hnow=Hnow+(auxvvl*3600);
    auxvvl = ui->lbl_Relogio->text().split("")[6].toInt();
    Hnow=Hnow+(auxvvl*10*60);
    auxvvl = ui->lbl_Relogio->text().split("")[7].toInt();
    Hnow=Hnow+(auxvvl*60);
    auxvvl = ui->lbl_Relogio->text().split("")[11].toInt();
    Hnow=Hnow+(auxvvl*10);
    auxvvl = ui->lbl_Relogio->text().split("")[12].toInt();
    Hnow=Hnow+(auxvvl);

    if (Hnow==vvl_m[0])
        VPLonga();
    else if (Hnow==vvl_m[1])
        VPCurta();
    else if (Hnow==vvl_m[2])
        VPLeitura();
    else if (Hnow==vvl_m[3])
        VPCurta();
    else if (Hnow==vvl_m[4])
        VPCurta();
    else if (Hnow==vvl_m[5])
        VPLonga();
    else if (Hnow==vvl_m[6])
        VPCurta();
    else if (Hnow==vvl_m[7])
        VPLonga();

    else if (Hnow==vvl_t[0])
        VPLonga();
    else if (Hnow==vvl_t[1])
        VPCurta();
    else if (Hnow==vvl_t[2])
        VPLeitura();
    else if (Hnow==vvl_t[3])
        VPCurta();
    else if (Hnow==vvl_t[4])
        VPCurta();
    else if (Hnow==vvl_t[5])
        VPLonga();
    else if (Hnow==vvl_t[6])
        VPCurta();
    else if (Hnow==vvl_t[7])
        VPLonga();

    else if (Hnow==vvl_n[0])
        VPLonga();
    else if (Hnow==vvl_n[1])
        VPCurta();
    else if (Hnow==vvl_n[2])
        VPCurta();
    else if (Hnow==vvl_n[3])
        VPCurta();
    else if (Hnow==vvl_n[4])
        VPLonga();
    else if (Hnow==vvl_n[5])
        VPCurta();
    else if (Hnow==vvl_n[6])
        VPLonga();

//Executa musica da leitura 5min antes.
    else if (Hnow==(vvl_m[2]-300))
        VPLeitura();
    else if (Hnow==(vvl_t[2]-300))
        VPLeitura();


}

SinalEscolar::~SinalEscolar()
{
    delete ui;
}


void SinalEscolar::on_btnSair_clicked()
{
    close();
}

void SinalEscolar::on_ck_Silencio_clicked()
{
    shii=!shii;

}

void SinalEscolar::on_btn_StopLonga_clicked()
{
    playLonga->stop();
    playCurta->stop();
    playLeitura->stop();
    playHino->stop();
    playSirene->stop();
}

void SinalEscolar::on_btn_PlayLonga_clicked()
{
    playLonga->setMedia(QUrl::fromLocalFile("/opt/Sinal/Musicas/entrada.mp3"));
    playLonga->play();
}

void SinalEscolar::on_btn_PlayCurta_clicked()
{
    playCurta->setMedia(QUrl::fromLocalFile("/opt/Sinal/Musicas/troca.mp3"));
    playCurta->play();
}

void SinalEscolar::on_btn_PlayLeit_clicked()
{
    playLeitura->setMedia(QUrl::fromLocalFile("/opt/Sinal/Musicas/leitura.mp3"));
    playLeitura->play();
}

void SinalEscolar::on_btn_PlayHino_clicked()
{
    playHino->setMedia(QUrl::fromLocalFile("/opt/Sinal/Musicas/hinobr.mp3"));
    playHino->play();
}

void SinalEscolar::on_btn_playSirene_clicked()
{
    playSirene->setMedia(QUrl::fromLocalFile("/opt/Sinal/Musicas/sirene.mp3"));
    playSirene->play();
}

void SinalEscolar::VPLonga()
{
    if (shii==0)
    {
    //Chama click do botão
     ui->btn_PlayLonga->click();
    }
}
void SinalEscolar::VPCurta()
{
    if (shii==0)
    {
    //Chama click do botão
     ui->btn_PlayCurta->click();
    }
}
void SinalEscolar::VPLeitura()
{
    if (shii==0)
    {
    //Chama click do botão
     ui->btn_PlayLeit->click();
    }
}

void SinalEscolar::on_btn_AltSongs_clicked()
{
    Musicas *frmMusicas = new Musicas();
    frmMusicas->show();
}

