#ifndef MUSICAS_H
#define MUSICAS_H

#include <QWidget>

namespace Ui {
class Musicas;
}

class Musicas : public QWidget
{
    Q_OBJECT

public:
    explicit Musicas(QWidget *parent = 0);
    ~Musicas();

private slots:
    void on_btn_Entrada_clicked();

    void on_pushButton_2_clicked();

    void on_btn_Troca_clicked();

    void on_btn_Leitura_clicked();

    void on_btn_TrMus_clicked();

private:
    Ui::Musicas *ui;
    QString OLonga;
    QString OLivro;
    QString OCurta;
};

#endif // MUSICAS_H
