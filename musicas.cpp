#include "musicas.h"
#include "ui_musicas.h"

#include <QFileDialog>
#include <QApplication>
#include <QDesktopWidget>
#include <QProcess>
#include <QFile>

Musicas::Musicas(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Musicas)
{
    ui->setupUi(this);
    QRect src = QApplication::desktop()->screenGeometry();
    move(src.center() - rect().center());
}

Musicas::~Musicas()
{
    delete ui;
}

void Musicas::on_btn_Entrada_clicked()
{
  QStringList flonga = QFileDialog::getOpenFileNames(this, tr("Open File"),"/path/to/file/",tr("Mp3 Files (*.mp3)"));
  //QStringList flonga = QFileDialog::getOpenFileNames(this, tr("Open File"),"/path/to/file/");
      if (flonga.isEmpty())
      {
          ui->lbl_entrada->setText("Sem Musica Selecionada");
      }
     else
      {
          OLonga = flonga[0];
          ui->lbl_entrada->setText(OLonga);
      }


}

void Musicas::on_pushButton_2_clicked()
{
    close();

}

void Musicas::on_btn_Troca_clicked()
{
    QStringList ftroca = QFileDialog::getOpenFileNames(this, tr("Open File"),"/path/to/file/",tr("Mp3 Files (*.mp3)"));
    //QStringList ftroca = QFileDialog::getOpenFileNames(this, tr("Selecione a Musica para troca de Aula"),"/path/to/file/");

      if (ftroca.isEmpty())
      {
         ui->lbl_troca->setText("Sem Musica Selecionada");
      }
     else
      {
          OCurta = ftroca[0];
          ui->lbl_troca->setText(OCurta);
      }
}

void Musicas::on_btn_Leitura_clicked()
{
    QStringList flivro = QFileDialog::getOpenFileNames(this, tr("Open File"),"/path/to/file/",tr("Mp3 Files (*.mp3)"));
      //QStringList flivro = QFileDialog::getOpenFileNames(this, tr("Open File"),"/path/to/file/");

      if (flivro.isEmpty())
      {
         ui->lbl_Leitura->setText("Sem Musica Selecionada");
      }
     else
      {
          OLivro = flivro[0];
          ui->lbl_Leitura->setText(OLivro);
      }
}


void Musicas::on_btn_TrMus_clicked()
{
    //Troca Musica de entrada
    if (ui->lbl_entrada->text()=="Sem Musica Selecionada" || ui->lbl_entrada->text()=="Selecione a Música de entrada:")
    {
        ui->lbl_entrada->setText("Música de entrada não selecionada");
    }
    else
    {
        QFile::remove("/opt/Sinal/Musicas/entrada-bkp.mp3");
        QFile::rename("/opt/Sinal/Musicas/entrada.mp3","/opt/Sinal/Musicas/entrada-bkp.mp3");
        QFile::rename(OLonga,"/opt/Sinal/Musicas/entrada.mp3");
        ui->lbl_entrada->setText("Música de Entrada alterada com sucesso!");
    }

    //Troca Musica de Troca
    if (ui->lbl_troca->text()=="Sem Musica Selecionada" || ui->lbl_troca->text()=="Selecione a Música de troca de Aula:")
    {
        ui->lbl_troca->setText("Música de troca não selecionada");
    }
    else
    {
        QFile::remove("/opt/Sinal/Musicas/troca-bkp.mp3");
        QFile::rename("/opt/Sinal/Musicas/troca.mp3","/opt/Sinal/Musicas/troca-bkp.mp3");
        QFile::rename(OCurta,"/opt/Sinal/Musicas/troca.mp3");
        ui->lbl_troca->setText("Música de Troca alterada com sucesso!");
    }

    //Troca Musica de leitura
    if (ui->lbl_Leitura->text()=="Sem Musica Selecionada" || ui->lbl_Leitura->text()=="Selecione a Música de leitura:")
    {
        ui->lbl_Leitura->setText("Música de leitura não selecionada");
    }
    else
    {
        QFile::remove("/opt/Sinal/Musicas/leitura-bkp.mp3");
        QFile::rename("/opt/Sinal/Musicas/leitura.mp3","/opt/Sinal/Musicas/leitura-bkp.mp3");
        QFile::rename(OLivro,"/opt/Sinal/Musicas/leitura.mp3");
        ui->lbl_Leitura->setText("Música de leitura alterada com sucesso!");
    }

    close();
}
